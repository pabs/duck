duck (0.13.5) unstable; urgency=medium

  [ Paul Wise ]
  * Add more obsolete domains.
  * Add indicators of inactive projects.
  * Account for both spellings of superseded.

  [ Baptiste Beauplat ]
  * Drop unused lintian override license-problem-gfdl-invariants
  * Add lintian override for test/tests.txt
    very-long-line-length-in-source-file tag

 -- Baptiste Beauplat <lyknode@debian.org>  Tue, 16 Aug 2022 21:03:42 +0200

duck (0.13.4) unstable; urgency=medium

  [ Paul Wise ]
  * Add phrases indicating discontinued projects or dead website.
    Found-on:
    - https://www.keepassx.org/index.html%3Fp=636.html
    - https://www.policyd-weight.org/
    - https://gpsgames.org
    - https://github.com/bruceravel/horae
    - https://www.igniterealtime.org/projects/spark/index.jsp
    - https://detector.io/
    - https://github.com/googlefonts/sfntly
    - https://www.opsmate.com/titus/
    - https://www.galago-project.org/
  * Fix typos and handle both variants of commonly misspelled word "superseded"

  [ Baptiste Beauplat ]
  * Add perltidy profile
  * Re-indent and re-format perl code
  * Drop unused /usr/share/duck/lib to lib search path
  * Include FindBin::Bin before /usr/share/duck to facilitate development
  * Remove trailing lines and spaces from config files
  * Organize source into src, doc, config, man, lib and test subdirectories
  * Fix typo in changelog
  * Bump templates Standards-Version to 4.6.1
  * Drop duck.conf from example, already installed under /etc/duck
  * Add support for loading config file obsolete.list and parked.list
  * Move parked and obsolete data to respective files
  * Update copyright years in d/copyright
  * Add myself as copyright holder
  * Add COPYING
  * Add minimalist README.md
  * Bump VERSION in lib/DUCK.pm

 -- Baptiste Beauplat <lyknode@debian.org>  Sun, 15 May 2022 10:20:28 +0200

duck (0.13.3) unstable; urgency=medium

  [ Paul Wise ]
  * Add phrases indicating deprecated projects

  [ Baptiste Beauplat ]
  * Update my email info in d/control and d/copyright
  * Bump Standards-Version to 4.6.0: no changes

 -- Baptiste Beauplat <lyknode@debian.org>  Thu, 26 Aug 2021 19:45:35 +0200

duck (0.13.2) unstable; urgency=medium

  [ Paul Wise ]
  * Add more terms to indicate sites being shut down.
    Found-on:
    - https://bitbucket.org/shimizukawa/sphinxjp.themes.htmlslide
    - https://www.openlaszlo.org/
    - https://sourceforge.net/projects/bugle/
    - https://sourceforge.net/projects/bugle/
    - https://palantir.github.io/tslint/

  [ Baptiste Beauplat ]
  * Fix my last name in the Maintainer field
  * Add git.debian.org and anonscm.debian.org to the obsolete site list
  * Replace dead Homepage by salsa project page
  * Bump Standards-Version to 4.5.1: no changes
  * Complete copyright information:
    - Fix GPL-2.0+ identifier
    - Add License block for the GPL-2.0+
    - Fix copyright years and email for Simon Kainz
    - Add myself for copyright on debian/* files
  * Re-indent and cleanup whitespace and trailing lines from debian/* files
  * Add Salsa CI pipeline

 -- Baptiste Beauplat <lyknode@cilg.org>  Thu, 04 Feb 2021 18:49:38 +0100

duck (0.13.1) unstable; urgency=medium

  [ lintian-brush ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update Vcs-* headers to use salsa repository.

  [ Paul Wise ]
  * Wrap and sort Debian packaging
  * Orphan the package as Simon Kainz retired from Debian (see #960137)
    Thanks for all your work on DUCK Simon!
  * Fix some typos

  [ Baptiste BEAUPLAT ]
  * New maintainer (Closes: #960137)
  * Match parked_domains regexp using word boundaries to avoid arbitrary
    string to match (Closes: #953009)
  * Bump debhelper-compat (= 13): no changes
  * Bump std version to 4.5.0: no changes
  * Add Rules-Requires-Root: no
  * Fix lintian-override for license-problem-gfdl-invariants in
    test/tests.txt

 -- Baptiste BEAUPLAT <lyknode@cilg.org>  Sun, 17 May 2020 20:37:24 +0200

duck (0.13) unstable; urgency=low

  * Fix no-color display on some terminals
    (Thanks to gregoa)

  * Add regex "may now be found", "no longer.* maintain",
    "future development will", "now hosted" regexes
    (Thanks to pabs)

  * Do not mention lynx in the error message when dget fails
    The command being run is dget not lynx.
    (Thanks to pabs)

   * warn about obsolete/read-only sites
     Add new section [obsolete_sites] in duck.conf
     (Closes: #851847)

   * warn about codeplex becoming obsolete. (Closes: #859249)

   * Fix "Use of uninitialized value" on domain redirection
     check. (Closes: #867279)

   * Add new option -l <filename> which allows checking all
     urls, email domains, git:// and svn:// entries in said file.
     (Closes: #838168)

 -- Simon Kainz <skainz@debian.org>  Thu, 10 Aug 2017 22:29:31 +0200

duck (0.12) unstable; urgency=low

  * Run checks in parallel.

  * Add new parameter --tasks to specify how many checks may be
    started in parallel - Default value is 24.

  * Do not show colors on dumb terminals (e.g. M-x shell in Emacs)
    (Closes: #843948)

  * Do not use hardcoded /tmp/... path for temporary directories
    (Closes: #838169)

  * Fix parsing of appstream-metadata files, handle screenshot elements
    with additional data. (Closes: #838167)

  * Fix possible include path issues during build time.
    Thanks to Dominic Hargreaves <dom@earth.li> for the patch.
    (Closes: #834520)

  * Verify SSL certificates against ca-certificates. (Closes: #826694)

  * Add new command line option --no-check-certificate to enable ignoring
    SSL failures on browser-style checks. (Closes: #826694)

 -- Simon Kainz <skainz@debian.org>  Mon, 12 Dec 2016 21:35:55 +0100

duck (0.10) unstable; urgency=high

  * fix warnings concerning uninitialized variables (Closes: #826439)

  * fix CVE-2016-1239: loads arbitrary code from the current untrusted
    directory

  * Detect back-redirection from HTTPS -> HTTP for HTTP vs. HTTPS
    similarity check (Closes: #827707)

 -- Simon Kainz <skainz@debian.org>  Mon, 04 Jul 2016 23:48:41 +0200

duck (0.9) unstable; urgency=medium

  * Fix warnings and infinite recursion (Closes: #824053)

  * Fix typos in manpage.

  * Set standards-version to 3.8.9

  * Add lintian overrides for test data files.

 -- Simon Kainz <skainz@debian.org>  Thu, 12 May 2016 09:42:36 +0200

duck (0.8) unstable; urgency=medium

  * Update Maintainer: field.

  * Fix handling Message-Id:'s as emails (Closes: #786631)

  * Improve email extractor. (Closes: #786632)

  * Change SVN-url detection in copyright-files (Closes: #786687)

  * Improve URL extraction (Closes: #787283) (Closes: #786686)
    (Closes: #787283)

  * Strip HTML comment for website-moved checks (Closes: #786761)

  * Improve URL extraction (Closes: #787270)

  * Add new command line option:

    --disable-urlfix=...	disable specified url fix function(s)
    --enable-urlfix=...		disable specified url fix function(s)

  * New feature: duck is now able to download and process .dsc files.

  * Add new command line option:

    --color=[auto,always,newer]  (Closes: #797480)

  * Remove command line option --no-color in favour of --color

  * Report domain redirects and HTTPS/HTTP schema changes. (Closes: #801226)

 -- Simon Kainz <skainz@debian.org>  Mon, 09 May 2016 15:16:41 +0200

duck (0.7) unstable; urgency=medium

  * Change certainty level (certain -> wild-guess) and
    warning level (Error -> Information) for checks on
    outdated websites (Closes: #766434)

  * Set Standards-Version to 3.9.6

 -- Simon Kainz <simon@familiekainz.at>  Thu, 23 Oct 2014 08:27:21 +0200

duck (0.6) unstable; urgency=low

  * Improved SSL handling for https URLS, now
    several SSL versions as well as GnuTLS is tried (Closes: #763069)

  * Add checks for documentation urls
    in systemd.unit files.

  * Fix Perl 5.20.* warning on experimental
    features. (Closes: #763068)

  * Change package dependencies from Depends: to
    Suggests: for external helpers.

  * Add very simple autodetection for SVN URLs
    in copyright files. (Closes: #743793)

  * Add checks for Appstream Metadata files.

  * Add environment variables: DUCK_NOCOLOR and DUCK_NOHTTPS

  * Add colorized output (use --no-color to disable).

  * for http URLs, add check if the equivalent https URL works (Closes: #746932)

  * Now trailing commas are not treated as part of URLs
    in debian/copyright file (Closes: #751165)

  * New output format: Prefixes for OK entries (O:),
    Warning entries (W:) and Error entries (E:).

  * Extend regex matching scheme for d/control files.
    Now matches HTTP, HTTPS and FTP also. (Closes: #751160)

  * Enhance detection for expired domains/domains for sale. (Closes: #747149)
    Thanks to Paul Wise <pabs@debian.org> for this.

  * Fix spurious newlines between lines of helper output (Closes: #741549)

  * Add processing of DEP-3 patch files in directories containing
    a series file. (Closes: #742000)

  * Add new command line option:

     -P     skip processing of patch files.

  * Improved error handling for unreadable files. (Closes: #742252)

 -- Simon Kainz <simon@familiekainz.at>  Tue, 14 Oct 2014 20:24:47 +0200

duck (0.5) unstable; urgency=medium

  * Add support for Emails and URLs in debian/control.
  * Fix leftover dependencies on libparse-debian-packages-perl.
  * Add new command line option:

       -n            dry run, don't run any checks, just show
                     what would be checked

  * Add info for gbp buildpackage hook in README.Debian
  * Use libregexp-common-perl and
    libregexp-common-email-address-perl to improve detection
    of URLs and email addresses.

 -- Simon Kainz <simon@familiekainz.at>  Thu, 13 Mar 2014 13:53:05 +0100

duck (0.4) unstable; urgency=low

  * Add support for mailto: URLS (Closes: #740862)

  * Fix Architecture: entry in debian/control (Closes: #740997)

  * Add check for email domains in Maintainer: field

  * Add check for email domains in Uploaders: field

  * Change parsing backend to libparse-debcontrol-perl (Closes: #740899)

  * Add minimal URL scheme detection for Repository: field
    in upstream metadata (Closes: #740859)

  * Add new command line options:

       -F     skip processing of the control file.
       -u     specify path to upstream metadata file.
       -U     skip processing of the upstream metadata file.

  * Fixed URL handling for VCS-Git Urls: -b option now parsed correctly.

 -- Simon Kainz <simon@familiekainz.at>  Tue, 11 Mar 2014 09:01:11 +0100

duck (0.3) unstable; urgency=low

  * Add Missing dependency libyaml-libyaml-perl (Closes: #740923)

 -- Simon Kainz <simon@familiekainz.at>  Thu, 06 Mar 2014 10:23:09 +0100

duck (0.2) unstable; urgency=low

  * Remove convenience copy of libparse-debian-packages-perl.
  * Change dh version to 9.
  * Minor code cleanup.
  * Add new check for Vcs-Bzr: entries

 -- Simon Kainz <simon@familiekainz.at>  Tue, 04 Mar 2014 08:40:40 +0100

duck (0.1) unstable; urgency=low

  * Initial release. (Closes: #739483)

 -- Simon Kainz <simon@familiekainz.at>  Wed, 19 Feb 2014 09:50:04 +0000
