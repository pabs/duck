# duck - the Debian Url ChecKer

duck is a QA tool that processes several fields in the debian/control,
debian/upstream, debian/copyright, debian/patches/\* and systemd.unit files and
checks if URLs, VCS links and email address domains found therein are valid.

This project is available under the terms of the GPL-2.0+ and the source is
hosted on https://salsa.debian.org/debian/duck.
